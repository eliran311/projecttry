import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassifiedArticlesComponent } from './classified-articles.component';

describe('ClassifiedArticlesComponent', () => {
  let component: ClassifiedArticlesComponent;
  let fixture: ComponentFixture<ClassifiedArticlesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassifiedArticlesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassifiedArticlesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
