import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';



@Injectable({
  providedIn: 'root'
})
export class MoviesService {
  apikey: string;
  private itemsCollection: AngularFirestoreCollection;

  constructor(private http: HttpClient, private db: AngularFirestore) { 
    this.apikey = 'fed69657ba4cc6e1078d2a6a95f51c8c';
    // this.itemsCollection = db.collection('comments',);
   
  }
    
 


   getPopular() {
   let header= {'Content-Type': 'application/json'}
    let param = {}
    param["sort_by"]= 'popularity.desc'
    param["api_key"]= this.apikey

    return this.http.get('https://api.themoviedb.org/3/discover/movie?',
    {headers:header, params:param})
     
      
  }

  getMovie(id: string) {
    let header= {'Content-Type': 'application/json'}
    let param = {}
    param["sort_by"]= 'popularity.desc'
    param["api_key"]= this.apikey

    return this.http.get('https://api.themoviedb.org/3/movie/'+ id +'?',{headers:header, params:param})
  
  }

  getMovieReviews(id: string) {
    let header= {'Content-Type': 'application/json'}
    let param = {}
    param["api_key"]= this.apikey
    return this.http.get('https://api.themoviedb.org/3/movie/'+ id +'/reviews?', {headers:header, params:param})
    

      }
      getMovieCredits(id: string) {
        let header= {'Content-Type': 'application/json'}
        let param = {}
        param["sort_by"]= 'popularity.desc'
        param["api_key"]= this.apikey
        return this.http.get('https://api.themoviedb.org/3/movie/'+ id +'/credits?', {headers:header, params:param})
         
      }

getcomments(movieId){
 
  this.itemsCollection = this.db.collection('comments', ref => ref.where('movieId', '==', movieId));
    var response = this.itemsCollection.valueChanges();
    return response;
  };
 
  







      
  }



