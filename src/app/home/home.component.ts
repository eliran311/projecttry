import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MoviesService } from '../movies.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  title: string;
  movies: [];

  constructor(private moviesServices: MoviesService,
    private router: ActivatedRoute) { }
  
  ngOnInit() {
 
      this.moviesServices.getPopular().subscribe(res => {
        console.log(res);
        console.log(res['results']);
        this.movies = res['results'];
      });
   
  }
  }


