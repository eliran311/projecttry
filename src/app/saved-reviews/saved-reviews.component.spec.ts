import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavedReviewsComponent } from './saved-reviews.component';

describe('SavedReviewsComponent', () => {
  let component: SavedReviewsComponent;
  let fixture: ComponentFixture<SavedReviewsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavedReviewsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedReviewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
