import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../movies.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ClassifyService } from '../classify.service';


@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit {
  movie: Object;
  reviews: Array<Object>;
  cast: Array<Object>;
    // comments$:Observable<any>;
  


  constructor(private classify:ClassifyService, private _moviesServices: MoviesService,private routers:Router, private router: ActivatedRoute) { }

  classifyThisReview(review){   
    this.classify.movieId = this.router.snapshot.params.id;
    console.log(this.classify.movieId);
    console.log(review)
    this.classify.doc = review;
    this.classify.movieName = this.movie['title']
    this.routers.navigate(['/classified_articles']);
  }

  ngOnInit() {
    this.router.params.subscribe((params) => {
      const id = params['id'];
      this._moviesServices.getMovie(id).subscribe(movie => {
        this.movie = movie;
    
      });
      this._moviesServices.getMovieReviews(id).subscribe(reviews => {
        console.log(reviews['results'])
        this.reviews = reviews['results'];
      });
      this._moviesServices.getMovieCredits(id).subscribe(res => {
        res['cast'] = res['cast'].filter((item) => {return item.profile_path});
        this.cast = res['cast'].slice(0,4);
      });
    });
    }

  



  }
