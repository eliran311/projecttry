import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
// import { ImageService } from '../image.service';


@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {
  Comments$: Comment[];
  comment:string;
  comments:any;
  comments$:Observable<any>;
  url:string;
  text:string;
  Movie:string;
  
  constructor( private service:ClassifyService,private classify:ClassifyService,private router:Router,private route: ActivatedRoute ) { }
  
  onSubmit(){
    this.classify.movieId = this.route.snapshot.params.id;
    this.classify.doc = this.text;
    this.classify.movieName = this.Movie;
    this.router.navigate(['/classified_articles']);
  }


  ngOnInit() {
    this.service.getComment()
    .subscribe(data =>
      // this.Comments$ = data
      console.log("data:" + data)
      );//הדטה שאנחנו מקבלים מהפונקציה גט פוסט ייכנס למשתנה פוסט
 
   }





  

}
