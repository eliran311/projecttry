// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig : {
    apiKey: "AIzaSyAvKFSynnu0KHyZpg0anb4yGD5lKyPd1Ds",
    authDomain: "projecttry-1bf56.firebaseapp.com",
    databaseURL: "https://projecttry-1bf56.firebaseio.com",
    projectId: "projecttry-1bf56",
    storageBucket: "projecttry-1bf56.appspot.com",
    messagingSenderId: "614022646202",
    appId: "1:614022646202:web:f0b67a8bcc9f5aec92b583",
    measurementId: "G-8HV2C5LVSB"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
